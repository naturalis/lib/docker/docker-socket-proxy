# Docker socket proxy

This is the upgraded docker socket proxy docker image used in many docker-compose
setups at Naturalis.

It is based on [tecnativa / docker-socket-proxy](https://github.com/Tecnativa/docker-socket-proxy)

This is a security-enhanced proxy for the Docker Socket.

> Giving access to your Docker socket could mean giving root access to your host, or even to your whole swarm, but some services require hooking into that socket to react to events, etc. Using this proxy lets you block anything you consider those services should not do.

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

 * `pre-commit autoupdate`
 * `pre-commit install`
